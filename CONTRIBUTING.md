# Contributing

Se vuoi partecipare al progetto puoi dare un'occhiata alle [issues](https://gitlab.com/animegg/animegg/issues) già aperte e provare a fornire una tua soluzione.
Altrimenti crea una nuova issue cercando di spiegare al meglio possibile cosa vorresti fixare, migliorare o aggiungere.

Se hai già un'idea di come risolvere la issue fai un fork della repo dal branch `master`, implementa la tua soluzione e condividila aprendo una [merge request](https://gitlab.com/animegg/animegg/merge_requests).
