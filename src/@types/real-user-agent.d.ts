declare module 'real-user-agent' {
	const userAgent: {
		(): Promise<string>;
		all(): Promise<string[]>;
		cycle(index?: number): Promise<string>;
	};
	export default userAgent;
}
