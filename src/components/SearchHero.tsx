import React, { useEffect, useRef } from 'react';

import classnames from 'classnames';
import { IoIosClose, IoIosSearch } from 'react-icons/io';

interface Props {
	query: string;
	onChange: (query: string) => void;
	onSubmit: () => void;
	onClear: () => void;
	isExpanded: boolean;
}

export const SearchHero: React.FC<Props> = ({
	query,
	onChange,
	onClear,
	onSubmit,
	isExpanded,
}) => {
	const inputRef = useRef<HTMLInputElement | null>(null);

	const focusInput = () => {
		inputRef.current?.focus();
	};

	useEffect(() => {
		focusInput();
	}, []);

	const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
		const { value } = event.currentTarget;
		onChange(value);
		// TODO search mentre input (+ setting per performance)
	};

	const handleClear = () => {
		onClear();
		focusInput();
	};

	return (
		<div
			className={classnames(
				'flex flex-col flex-shrink-0 items-center self-end px-4',
				isExpanded && 'p-4',
			)}
		>
			<h1
				className={classnames(
					'text-8xl font-hairline text-white text-center select-none text-shadow-on-light',
					!isExpanded && 'hidden',
				)}
			>
				Cerca anime
			</h1>
			<div
				className={classnames('w-full mx-auto h-10', isExpanded && 'h-12 px-6')}
			>
				<form
					className="flex flex-grow h-full w-full max-w-2xl no-draggable z-30 shadow-md rounded-full"
					onSubmit={onSubmit}
				>
					<input
						ref={inputRef}
						className="flex-grow pl-4 bg-white focus:outline-none font-light rounded-l-full"
						title='Es. "attacco", "jojo", ecc.'
						placeholder="Cerca anime per titolo"
						type="text"
						value={query}
						onChange={handleChange}
						onPaste={handleChange}
						autoFocus={true}
					/>
					<button
						className="w-8 text-xl bg-white"
						type="reset"
						onClick={handleClear}
					>
						<IoIosClose className="mx-auto" />
					</button>
					<button
						className="button button-primary flex-shrink-0 rounded-l-none rounded-r-full h-full"
						type="submit"
					>
						<span className="mr-2">Cerca</span> <IoIosSearch />
					</button>
				</form>
			</div>
		</div>
	);
};
