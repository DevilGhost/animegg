import React from 'react';

interface Props {
	run?: boolean;
	execute: () => void;
	ms: number;
}

export default class SmartInterval extends React.Component<Props> {
	private interval?: NodeJS.Timeout;

	componentDidMount() {
		if (this.shouldRun()) {
			this.startInterval();
		}
	}

	componentDidUpdate() {
		if (this.shouldRun()) {
			this.startInterval();
		} else {
			this.stopInterval();
		}
	}

	componentWillUnmount() {
		this.stopInterval();
	}

	startInterval() {
		if (!this.interval) {
			this.interval = global.setInterval(this.props.execute, this.props.ms);
		}
	}

	stopInterval() {
		if (this.interval) {
			global.clearInterval(this.interval);
			this.interval = undefined;
		}
	}

	render() {
		return this.props.children || null;
	}

	private shouldRun() {
		if ('run' in this.props) {
			return Boolean(this.props.run);
		}
		return true;
	}
}
