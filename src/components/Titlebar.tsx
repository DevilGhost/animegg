import React, { useState } from 'react';
import classNames from 'classnames';
import {
	IoMdRemove,
	IoMdClose,
	IoMdSquareOutline,
	IoIosSettings,
} from 'react-icons/io';
import { observer } from 'mobx-react';

import { useStores } from '../hooks/useStores';
import { APP_VERSION } from '../constants';

import { Modal } from './Modal';
import { Settings } from './Settings';

interface Props {
	isExpanded: boolean;
}

export const Titlebar: React.FC<Props> = observer((props) => {
	const { isExpanded } = props;
	const { ui } = useStores();

	const [showSettings, setShowSettings] = useState<boolean>(false);

	const showSettingsModal = () => {
		setShowSettings(true);
	};

	const hideSettingsModal = () => {
		setShowSettings(false);
	};

	// TODO gradient blur titlebar
	// es. https://codepen.io/iamvdo/pen/xECmI
	return (
		<div
			className={classNames(
				'flex flex-flow items-start w-full h-16 transition-all duration-300',
				isExpanded && 'h-screen-50',
			)}
		>
			<div className="relative flex-shrink-0 my-2 mx-4">
				<img
					className="h-8 w-auto"
					src="assets/images/logo.png"
					onClick={ui.useNextBackground}
				/>
				<span className="absolute right-0 bottom-0 -mb-3 text-xs text-gray-100 opacity-75">
					v{APP_VERSION}
				</span>
			</div>
			<div className="flex flex-grow p-3 justify-center self-stretch draggable">
				{props.children}
			</div>
			<div className="z-50 grid grid-cols-5 gap-2 p-4 items-center flex-shrink-0 text-2xl text-white icon-shadow-on-light">
				<button
					className="transition-colors duration-200 ease-in hover:text-primary"
					onClick={showSettingsModal}
					title="Impostazioni dell'app"
				>
					<IoIosSettings />
				</button>
				<span className="border-solid border-r-1 opacity-75 border-white w-1 h-full m-auto" />
				<button
					className="transition-colors duration-200 ease-in hover:text-primary"
					onClick={ui.minimizeWindow}
				>
					<IoMdRemove />
				</button>
				<button
					className="transition-colors duration-200 ease-in hover:text-primary"
					onClick={ui.maximizeWindow}
				>
					<IoMdSquareOutline />
				</button>
				<button
					className="transition-colors duration-200 ease-in hover:text-primary"
					onClick={ui.closeWindow}
				>
					<IoMdClose />
				</button>
			</div>
			<div className="no-draggable absolute inset-x-0 top-0 h-2" />
			<Modal show={showSettings} onHide={hideSettingsModal}>
				<Settings />
			</Modal>
		</div>
	);
});
