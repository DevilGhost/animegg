import React, { useEffect } from 'react';
import { useHistory, useParams } from 'react-router';
import { observer } from 'mobx-react';
import { IoIosClose } from 'react-icons/io';

import { EpisodeType } from '../search/types';
import { useStores } from '../hooks/useStores';
import { groupBy } from '../utils/array';

import ScrollableContent from '../components/ScrollableContent';
import { AnimeDownloadActions } from '../components/AnimeDownloadActions';
import { EpisodeDownloadEntry } from '../components/EpisodeDownloadEntry';
import { EpisodeDownloadSection } from '../components/EpisodeDownloadSection';
import AnimeDetailsSidebar from '../components/AnimeDetailsSidebar';

export const AnimeDownloadPage: React.FC = observer(() => {
	const { animedl } = useStores();

	const history = useHistory();
	const { animeId } = useParams<{ animeId?: string }>();

	const animeDownload = animeId ? animedl.animes.get(animeId) : undefined;
	const anime = animeDownload?.anime;

	useEffect(() => {
		if (!anime && animeDownload) {
			animeDownload.fetchAnime();
		}
	}, [animeDownload]);

	if (!anime || !animeDownload) {
		return (
			<div className="flex flex-grow h-full bg-white">
				<div className="flex flex-col w-full h-full max-w-5xl px-4 self-center">
					<div className="flex flex-shrink-0 items-center w-full mt-3 mb-2">
						<div className="flex-grow" />
						<div className="flex-shrink-0">
							<button
								className="button text-gray-600 p-0 w-10 h-10"
								onClick={history.goBack}
							>
								<IoIosClose className="h-full w-full" />
							</button>
						</div>
					</div>
					<div className="flex flex-grow items-center justify-center">
						<p className="text-3xl text-gray-500 text-center">
							{animeDownload?.isFetching
								? 'Caricamento in corso...'
								: "Impossibile caricare l'anime richiesto."}
						</p>
					</div>
				</div>
			</div>
		);
	}

	const seasonEpisodes = animeDownload.episodes.filter(
		(episode) => episode.type === EpisodeType.SEASON,
	);

	const otherEpisodes = animeDownload.episodes.filter(
		(episode) => episode.type !== EpisodeType.SEASON,
	);

	const seasonGroupedEpisodes = groupBy(
		seasonEpisodes,
		(ep) => ep.groupTitle || '',
	);

	return (
		<div className="flex flex-col flex-grow h-full bg-white">
			<div className="flex flex-col w-full h-full max-w-5xl px-4 self-center">
				<div className="flex flex-shrink-0 items-center w-full mt-3 mb-2">
					<h1 className="flex-grow text-3xl">{anime.title}</h1>
					<div className="flex-shrink-0">
						<button
							className="button text-gray-600 p-0 w-10 h-10"
							onClick={history.goBack}
						>
							<IoIosClose className="h-full w-full" />
						</button>
					</div>
				</div>
				<div
					className="grid gap-4 h-full overflow-hidden"
					style={{ gridTemplateColumns: '260px 1fr' }}
				>
					<AnimeDetailsSidebar anime={anime}>
						<AnimeDownloadActions animeId={anime.id} />
					</AnimeDetailsSidebar>

					<ScrollableContent>
						{Object.entries(seasonGroupedEpisodes).map(([title, episodes]) => (
							<EpisodeDownloadSection
								key={`group-${title}`}
								title={title || 'Episodi'}
								episodes={episodes}
								onAddAll={() => {
									animedl.addEpisodesToDownload(
										anime,
										episodes.map((ep) => ep.id),
									);
								}}
								onRemoveAll={() => {
									animedl.removeEpisodesFromDownload(
										episodes.map((ep) => ep.id),
									);
								}}
							>
								{!episodes.length && (
									<div className="flex border-b-1 border-solid border-gray-400 px-3 py-1">
										(Nessun episodio)
									</div>
								)}
								{episodes.map((episode, i) => (
									<div
										className="flex bg-white-alternate px-3 py-1"
										key={episode.id}
									>
										<EpisodeDownloadEntry anime={anime} episode={episode} />
									</div>
								))}
							</EpisodeDownloadSection>
						))}

						{!!otherEpisodes.length && (
							<EpisodeDownloadSection
								title="Altri: OAV, Film, Extra, Trailers, ecc"
								episodes={otherEpisodes}
								onAddAll={() => {
									animedl.addEpisodesToDownload(
										anime,
										otherEpisodes.map((ep) => ep.id),
									);
								}}
								onRemoveAll={() => {
									animedl.removeEpisodesFromDownload(
										otherEpisodes.map((ep) => ep.id),
									);
								}}
							>
								{otherEpisodes.map((episode, i) => (
									<div
										className="flex bg-white-alternate px-3 py-1"
										key={episode.id}
									>
										<EpisodeDownloadEntry anime={anime} episode={episode} />
									</div>
								))}
							</EpisodeDownloadSection>
						)}
					</ScrollableContent>
				</div>
			</div>
		</div>
	);
});
