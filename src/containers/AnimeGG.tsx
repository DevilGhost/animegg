import React, { useEffect, useState } from 'react';
import { Route, Switch, Router, useLocation, useHistory } from 'react-router';
import posed, { PoseGroup } from 'react-pose';
import { tween } from 'popmotion';
import { createMemoryHistory } from 'history';

import { AnimeDownloaderStore } from '../stores/AnimeDownloaderStore';
import { UIStore } from '../stores/UIStore';

import { Titlebar } from '../components/Titlebar';
import { SearchHero } from '../components/SearchHero';
import { DownloaderBar } from '../components/DownloaderBar';
import { Background } from '../components/Background';

import { useStores } from '../hooks/useStores';
import { StoresContext } from '../contexts';
import { Stores } from '../types';

import { HomePage } from './HomePage';
import { SearchResultsPage } from './SearchResultsPage';
import { DownloaderPage } from './DownloaderPage';
import { AnimeDownloadPage } from './AnimeDownloadPage';
import { observer } from 'mobx-react';

const App: React.FC = observer(() => {
	const { ui, animedl } = useStores();
	const location = useLocation();
	const history = useHistory();
	const [searchQuery, setSearchQuery] = useState<string>('');

	const isHome = location.pathname === '/';
	const expandSearch = isHome && !ui.showDownloader;

	useEffect(() => {
		// Se cambia path chiude il downloader (es. quando scelgo un anime dal downloader)
		ui.toggleDownloader(false);
	}, [location.key, ui]);

	useEffect(() => {
		animedl.init();
	}, []);

	const clearSearch = () => {
		setSearchQuery('');
		history.push('/');
		animedl.clearLastSearch();
	};

	const submitSearch = () => {
		history.push('/results');
		animedl.search(searchQuery);
	};

	return (
		<div className="flex flex-col h-full w-full overflow-hidden">
			<Background />
			<Titlebar isExpanded={expandSearch}>
				<SearchHero
					query={searchQuery}
					onChange={setSearchQuery}
					onClear={clearSearch}
					onSubmit={submitSearch}
					isExpanded={expandSearch}
				/>
			</Titlebar>
			<div className="flex flex-grow relative">
				<PoseGroup>
					<StackedOntop
						className="absolute inset-0 height-full width-full"
						key={location.key || 'main'}
					>
						<Switch location={location}>
							<Route exact={true} path="/" component={HomePage} />
							<Route path="/results" component={SearchResultsPage} />
							<Route
								path="/anime/:animeId"
								key={location.key}
								component={AnimeDownloadPage}
							/>
						</Switch>
					</StackedOntop>
					{ui.showDownloader && (
						<StackedOntop
							key="DownloaderPage"
							className="absolute inset-0 height-full width-full"
						>
							<DownloaderPage />
						</StackedOntop>
					)}
				</PoseGroup>
			</div>
			<DownloaderBar />
		</div>
	);
});

export const AnimeGG: React.FC = observer(() => {
	const [stores] = useState<Stores>(() => ({
		ui: new UIStore(),
		animedl: new AnimeDownloaderStore(),
	}));

	const [history] = useState(() => createMemoryHistory());

	return (
		<StoresContext.Provider value={stores}>
			<Router history={history}>
				<App />
			</Router>
		</StoresContext.Provider>
	);
});

const StackedOntop = posed.div({
	enter: { y: 0, transition: tween },
	exit: { y: '110%' },
});
