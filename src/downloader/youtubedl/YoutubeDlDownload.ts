import execa from 'execa';
import path from 'path';
import { BIN_DIR, IS_RELEASE } from '../../constants';
import { IS_WINDOWS } from '../../constants';
import {
	Download,
	DownloadId,
	DownloadStage,
	DownloadState,
	TerminalDownloadStage,
} from '../../downloader/types';
import ytdlFfmpegOutput from './YtdlFfmpegOutput';
import ytdlNativeHlsOutput from './YtdlNativeHlsOutput';

const youtubeDlPath = IS_RELEASE
	? path.resolve(BIN_DIR, 'youtube-dl.exe')
	: 'youtube-dl';
const ffmpegPath = IS_RELEASE ? path.resolve(BIN_DIR, 'ffmpeg.exe') : 'ffmpeg';

// https://github.com/rg3/youtube-dl#configuration
const defaultArgs = [
	// Non dovrebbe farlo di default, ma just to be sure
	'--no-call-home',
	// FIXME flags per il reset non workano
	// Resetta i download, non continuare da download parziale precedente
	'--no-continue',
	'--no-part',
	// Abortisce se mancano frammenti per completare il download
	'--abort-on-unavailable-fragment',
	// Path per le librerie di ffmpeg e ffprobe
	...(IS_RELEASE ? ['--ffmpeg-location', ffmpegPath] : []),
	// Utile perchè torna un output più interpretabile in stdout, per feedback
	// sul download corrente.
	// https://github.com/rg3/youtube-dl#should-i-add---hls-prefer-native-into-my-config
	'--hls-prefer-native',
	'--newline',
	// Evita di scaricare un intera playlist se un URL si riferisce ad una playlist
	// può succedere con i link da youtube da un video di una playlist
	'--no-playlist',
	// TESTING limita la velocità ad 1 Mb/s
	// '-r', '1M',
];

/**
 * Messaggio di default da mostrare all'utente se succede un errore inaspettato.
 */
const DEFAULT_ERROR_MSG =
	'Download fallito inaspettatamente (per maggiori info consultare i log)';

const RELEVANT_LINES = 20;

export class YoutubeDlDownload implements Download {
	readonly id: string;
	readonly filepath: string;
	readonly url: string;

	private state: DownloadState = {
		sizeDownloaded: 0,
		stage: DownloadStage.READY,
	};
	private runtime?: execa.ExecaChildProcess<string>;

	constructor(opts: { id: DownloadId; url: string; filepath: string }) {
		this.id = opts.id;
		this.filepath = opts.filepath;
		this.url = opts.url;
	}

	async run(): Promise<TerminalDownloadStage> {
		if (this.runtime) {
			await this.cancel();
		}
		// TODO: stop, reset and run anew if already running, to reuse it?

		console.log('Running youtube-dl', this.id, this.url);
		const ytdl = execa(youtubeDlPath, [
			...defaultArgs,
			'-o',
			this.filepath,
			this.url,
		]);

		ytdl.stdout?.on('data', this.updateStateFromChunk(ytdlNativeHlsOutput));
		ytdl.stderr?.on('data', this.updateStateFromChunk(ytdlFfmpegOutput));

		this.runtime = ytdl;

		try {
			this.state = {
				stage: DownloadStage.DOWNLOADING,
				sizeDownloaded: 0,
				sizeTotal: 0,
				perc: 0,
				timeRemaining: 0,
			};

			await ytdl;

			const sizeTotal = this.state.sizeTotal || this.state.sizeDownloaded;
			this.state = {
				stage: DownloadStage.COMPLETED,
				sizeDownloaded: sizeTotal,
				sizeTotal: sizeTotal,
				perc: 100,
				timeRemaining: 0,
			};
			return DownloadStage.COMPLETED;
		} catch (err) {
			// err: ExecaError
			const stopped = err.isCanceled || err.killed;
			if (stopped) {
				this.state.stage = DownloadStage.STOPPED;
				return DownloadStage.STOPPED;
			}

			const relevantStderr = getFirstListLines(err.stderr, RELEVANT_LINES);
			const relevantStdout = getFirstListLines(err.stdout, RELEVANT_LINES);
			console.warn(
				'Failed youtube-dl',
				this.id,
				this.url,
				`\nSTDERR: ${relevantStderr}\n---\nSTDOUT: ${relevantStdout}`,
			);

			this.state.message = DEFAULT_ERROR_MSG;
			this.state.stage = DownloadStage.FAILED;
			return DownloadStage.FAILED;
		}
	}

	async cancel(): Promise<void> {
		console.log('Canceling youtube-dl', this.id, this.url);
		try {
			this.runtime?.cancel();
		} catch (err) {
			// HACK Catch TypeError because execa#kill uses undefined Timeout#unref
			// FIXME Timeout#unref, available only in node or electron main, not renderer
		}
	}

	getState(): DownloadState {
		return {
			...this.state,
		};
	}

	private updateStateFromChunk = (
		reader: (state: DownloadState, output: string) => DownloadState,
	) => (chunk?: string | Buffer) => {
		const output = chunk ? chunk.toString() : '';
		this.state = reader(this.state, output);
	};
}

function getFirstListLines(str: string, limit: number): string {
	const lines = str.split(/[\n\r]+/); // skips empty lines as well

	if (lines.length < limit * 2) {
		return str;
	}

	return [
		// head
		...lines.slice(0, limit),
		'...',
		// tail
		...lines.slice(lines.length - limit, lines.length),
	].join('\n');
}
