import { app, BrowserWindow, nativeImage } from 'electron';
import windowStateKeeper from 'electron-window-state';
import installExtension, {
	REACT_DEVELOPER_TOOLS,
} from 'electron-devtools-installer';

import { IS_DEV, IS_DEBUG } from './constants';
import { IS_RELEASE } from './constants';
import { getUserAgent } from './utils/net';

const WINDOW_MIN_WIDTH = 1024;
const WINDOW_MIN_HEIGHT = 768;

let win: BrowserWindow | undefined;

function appReady() {
	const mainWindowState = windowStateKeeper({
		defaultWidth: WINDOW_MIN_WIDTH,
		defaultHeight: WINDOW_MIN_HEIGHT,
	});

	win = new BrowserWindow({
		// show later, when ready
		show: false,
		// window options
		x: mainWindowState.x,
		y: mainWindowState.y,
		minWidth: WINDOW_MIN_WIDTH,
		minHeight: WINDOW_MIN_HEIGHT,
		width: mainWindowState.width,
		height: mainWindowState.height,
		fullscreenable: false,
		// eye candy
		frame: IS_DEBUG,
		icon: nativeImage.createFromPath('assets/images/icon-128.png'),
		backgroundColor: '#ffffff',
		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule: true,
		},
	});

	mainWindowState.manage(win);

	// Emitted when the window is closed.
	win.on('closed', () => {
		win = undefined;
	});

	win.once('ready-to-show', () => {
		if (win) {
			win.show();
			win.focus();
		}
	});

	const preventDefault = (event: Event) => {
		event.preventDefault();
	};

	// Enforces single-app
	win.webContents.on('will-navigate', preventDefault);
	win.webContents.on('new-window', preventDefault);

	// More real requests; fix Cloudflare images request
	win.webContents.session.webRequest.onBeforeSendHeaders(
		{
			urls: ['<all_urls>'],
		},
		(details, callback) => {
			details.requestHeaders['User-Agent'] = getUserAgent();
			delete details.requestHeaders['Referer'];
			callback({ requestHeaders: details.requestHeaders });
		},
	);

	if (IS_DEV) {
		win.webContents.openDevTools();
		Promise.all([
			installExtension(REACT_DEVELOPER_TOOLS),
			// Not working b/c error: "Cannot read property 'network' of undefined"
			// installExtension(MOBX_DEVTOOLS),
		]).catch(console.error); // eslint-disable-line no-console
	}

	win.loadURL(
		IS_RELEASE
			? `file://${__dirname}/renderer.html`
			: 'http://localhost:9000/renderer.html',
	);
}

app.on('window-all-closed', () => {
	// Respect the OSX convention of having the application in memory even
	// after all windows have been closed
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

const singleAppLock = app.requestSingleInstanceLock();

if (!singleAppLock) {
	app.quit();
} else {
	app.on('second-instance', () => {
		// Someone tried to run a second instance, we should focus our window.
		if (win) {
			if (win.isMinimized()) {
				// Restore su linux non fa niente, basta show.
				win.restore();
			}

			if (!win.isVisible()) {
				// Necessario, almeno su linux, per mostrare la finestra, se è
				// stata minimizzata prima.
				win.show();
			}

			win.focus();
		}
	});

	// Create mainWindow, load the rest of the app, etc...
	app.on('ready', appReady);
}
