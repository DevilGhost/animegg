import React from 'react';
import ReactDOM from 'react-dom';

import './renderer.css';

import { AnimeGG } from './containers/AnimeGG';
import * as constants from './constants';

window.onload = () => {
	console.log('Starting app', constants.APP_NAME, constants.APP_VERSION);
	console.log('App config', JSON.parse(JSON.stringify(constants)));

	const appContainer = document.getElementById('app');
	ReactDOM.render(<AnimeGG />, appContainer);
};
