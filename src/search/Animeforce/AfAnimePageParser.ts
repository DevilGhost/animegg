import cheerio from 'cheerio';

import AfUtils from './AfUtils';

interface AfEpisode {
	title: string;
	downloadUrl: string;
}

export default class AfAnimePageParser {
	static parse(page: string): AfAnimePageParser {
		const parsed = cheerio.load(page);
		return new AfAnimePageParser(parsed);
	}

	private $animePage: cheerio.Selector;
	private infoRows?: Map<string, cheerio.Cheerio>;

	private constructor(page: cheerio.Selector) {
		this.$animePage = page;
	}

	extractCoverUrl(): string | undefined {
		// TODO support new page format

		const coverUrl = this.$animePage('.the-content p img.aligncenter').attr(
			'src',
		);
		if (coverUrl) {
			return AfUtils.normalizeUrl(coverUrl);
		}
	}

	extractSubberInfo() {
		// TODO support new page format

		const infoRows = this.extractInfoRows();

		// "Fansub" in https://ww1.animeforce.org/ajin-sub-ita-download-streaming/
		// "Release" in https://ww1.animeforce.org/3-gatsu-no-lion-sub-ita-download-streaming/
		const elem = infoRows.get('Fansub') || infoRows.get('Release');

		if (!elem) {
			return {};
		}

		const link = elem.find('a');
		const linkHref = link.attr('href');
		const linkText = link.text();

		if (linkHref && linkText) {
			return {
				subberName: linkText,
				subberUrl: linkHref,
			};
		}
	}

	extractEpisodes(): AfEpisode[] {
		// TODO Separate Special, Movie and OAV from Season episodes
		return this.$animePage('.servers-container #nav-tabContent a')
			.toArray()
			.map((link) => ({
				title: link.attribs['title'],
				downloadUrl: link.attribs['href'],
			}))
			.filter((episode) => episode.title || episode.downloadUrl);
	}

	// Old page format, some page still use this
	private oldExtractEpisodes(): AfEpisode[] {
		// Selettore stagioni (skip prima tabella con info e no link eps)
		const SEASONS_SEL = '.the-content table:nth-of-type(1n+2)';
		// Selettore episodi di stagione (skip prima linea)
		const EPISODES_SEL = 'tr:nth-child(1n+2)';

		// Seleziona tutti gli episodi (in ogni stagione) dalla tabella principale
		return (
			this.$animePage(`${SEASONS_SEL} ${EPISODES_SEL}`)
				.toArray()
				.map((tr) => {
					const title = this.$animePage('td:first-child', tr).text();
					const download = this.$animePage('td:nth-child(2) a', tr).attr(
						'href',
					);
					const stream = this.$animePage('td:nth-child(3) a', tr).attr('href');

					return {
						title,
						// In media i link di download e stream sono uguali
						// Se il selettore del download o dello stream fallisce torna ''
						// (stringa vuota), ma può essere rimpiazzato l'uno con l'altro
						downloadUrl: download || stream,
					};
				})
				// Filtra episodi senza titolo o download/stream url
				.filter((ep): ep is AfEpisode => Boolean(ep.title && ep.downloadUrl))
		);
	}

	private extractInfoRows() {
		if (!this.infoRows) {
			const infoRows = new Map();

			this.$animePage('.the-content table:first-of-type tr')
				.toArray()
				.forEach((tr) => {
					const type = this.$animePage('td:first-child', tr).text().trim();
					const elem = this.$animePage('td:nth-child(2)', tr);

					if (type && elem) {
						infoRows.set(type, elem);
					}
				});

			this.infoRows = infoRows;
		}

		return this.infoRows;
	}
}
