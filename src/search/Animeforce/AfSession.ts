import got from 'got';
import userAgent from 'real-user-agent';
import { CookieJar } from 'tough-cookie';

export default class AfSession {
	private cookieJar = new CookieJar();
	private userAgent = '';

	async requestRaw(url: string, referer: string): Promise<string> {
		if (this.userAgent === '') {
			this.userAgent = await userAgent();
		}

		try {
			return (
				await got(url, {
					cookieJar: this.cookieJar,
					decompress: false,
					headers: {
						Host: 'ww1.animeforce.org',
						Referer: referer,
						Accept:
							'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Language': 'it-IT,it;q=0.8,en-US;q=0.6,en;q=0.4',
						Connection: 'keep-alive',
						'User-Agent': this.userAgent,
					},
				})
			).body;
		} catch (err) {
			throw new Error(`fallito caricamento url ${url}: ${err.message}`);
		}
	}
}
