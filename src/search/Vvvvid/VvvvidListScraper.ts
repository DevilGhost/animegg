import VvvvidShowAnimeScraper, {
	VvvvidAnimeResponse,
} from './VvvvidShowAnimeScraper';
import VvvvidSession from './VvvvidSession';
import VvvvidUtils from './VvvvidUtils';
import { AnimeOrigin, AnimeListScraper } from '../types';
import { pMap } from '../../utils/promise';
import { flatten } from '../../utils/array';

// Anime senza lettere solitamente: q, r, w, x
// Le includo lo stesso, nel caso aggiungano nuovi anime con quelle lettere.
const EXPECTED_EMPTY_LETTERS = ['q', 'r', 'w', 'x'];

export default class VvvvidListScraper implements AnimeListScraper {
	origin = AnimeOrigin.VVVVID;

	private session: VvvvidSession;

	constructor(session: VvvvidSession) {
		this.session = session;
	}

	async fetchList(): Promise<VvvvidShowAnimeScraper[]> {
		// If throws, cancel the operation; login here to prevent multiple logins
		await this.session.login();

		const rawList = await this.fetchListFromLetters(
			// search on the whole alphabet
			'abcdefghijklmnopqrstuvwxyz'.split(''),
		);

		// removes empty anime, without title or show_id
		const cleaned = rawList.filter(
			(anime) => typeof anime === 'object' && anime.title && anime.show_id,
		);

		const scrapers = cleaned.map(
			(anime) =>
				new VvvvidShowAnimeScraper({
					showId: anime.show_id,
					title: anime.title,
					url: VvvvidUtils.composeShowUrl(anime.show_id, anime.title),
					session: this.session,
					vvvvidAnime: anime,
				}),
		);

		return scrapers;
	}

	/**
	 * Fetch and aggregates a list of every animes, under every alphabet letter.
	 */
	async fetchListFromLetters(
		letters: string[],
	): Promise<VvvvidAnimeResponse[]> {
		const animes = await pMap(
			letters,
			async (letter) => {
				const letterShouldHaveAnime = !EXPECTED_EMPTY_LETTERS.includes(letter);
				let retries = 3;

				do {
					try {
						const animesFound = await this.fetchAnimesFromLetter(letter);
						return animesFound;
					} catch (err) {
						if (letterShouldHaveAnime && retries > 0) {
							retries -= 1;
						} else {
							break;
						}
					}
				} while (retries > 0 && letterShouldHaveAnime);
			},
			{ concurrency: 3 },
		);

		// concat every letter's list of animes
		return flatten(animes).filter(
			(res): res is VvvvidAnimeResponse => res !== undefined,
		);
	}

	/**
	 * Fetch animes by the capital letter.
	 * There are two API URLs that provide animes:
	 * - .../10003/last...
	 * - .../10003/... (others; no results since 2018/01/05)
	 */
	private async fetchAnimesFromLetter(letter: string) {
		const connId = await this.session.login();
		const queryParams = `?filter=${letter}&conn_id=${connId}`;

		// ex. http://www.vvvvid.it/vvvvid/ondemand/anime/channel/10003{/last,}?filter=a&conn_id=...
		// fetch from /last animes
		const letterPath = `vvvvid/ondemand/anime/channel/10003/last${queryParams}`;
		const animes = (await this.session.get(
			letterPath,
		)) as VvvvidAnimeResponse[];
		if (!animes.length) {
			throw new Error('Nessun anime trovato');
		}
		return animes;
	}
}
