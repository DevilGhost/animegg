import { AnimeListScraper, AnimeOrigin } from '../types';

import YtUtils from './YtUtils';
import YtSession from './YtSession';
import YtPlaylistsExtractor, { YtPlaylist } from './YtPlaylistsExtractor';
import YtPlaylistAnimeScraper from './YtPlaylistAnimeScraper';
import { flatten } from '../../utils/array';

interface YtPlaylistWithChannel extends YtPlaylist {
	channelId: string;
	channelName: string;
}

export interface YtChannel {
	/** Channel name */
	name: string;
	/** Channel Id; found via https://johnnythetank.github.io/youtube-channel-name-converter/ */
	id: string;
}

export default class YtListScraper implements AnimeListScraper {
	origin = AnimeOrigin.YOUTUBE;

	private channels: YtChannel[];
	private session: YtSession;

	constructor(channels: YtChannel[], session = new YtSession()) {
		this.channels = channels;
		this.session = session;
	}

	async fetchList(): Promise<YtPlaylistAnimeScraper[]> {
		const playlistsScraped = await Promise.all(
			this.channels.map((channel) => this.fetchPlaylists(channel)),
		);

		const playlists = flatten(playlistsScraped);

		// Some playlists might be duplicated
		const uniquePlaylists = playlists.filter(
			(playlist, i, a) => a.findIndex((apl) => apl.id === playlist.id) === i,
		);

		return uniquePlaylists.map(
			(playlist) =>
				new YtPlaylistAnimeScraper({
					...playlist,
					playlistId: playlist.id,
				}),
		);
	}

	// NOTE se possibile in futuro preferire utilizzo di youtube-dl invece che
	// reinventare la ruota.
	// Ma `youtube-dl -j --flat-playlists` non torna i "titoli" delle playlists
	// e senza `--flat-playlists` fa fetching completo e ci mette tanto con
	// molte playlist
	private async fetchPlaylists(
		channel: YtChannel,
	): Promise<YtPlaylistWithChannel[]> {
		const channelUrl = YtUtils.channelUrlFromId(channel.id);

		const playlistsUrl = `${channelUrl}/playlists`;
		const playlistsPage = await this.session.requestRaw(playlistsUrl);
		return new YtPlaylistsExtractor(playlistsPage)
			.extractPlaylists()
			.map((playlist) => ({
				...playlist,
				channelName: channel.name,
				channelId: channel.id,
			}));
	}
}
