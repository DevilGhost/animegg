import cheerio from 'cheerio';

import YtUtils from './YtUtils';

export interface YtPlaylist {
	/**
	 * L'Id della playlist
	 */
	id: string;
	title: string;
}

/**
 * Patterns per escludere le playlist che non sono di anime sul canale Yamato.
 */
const EXCLUDE_TITLES_YAMATO = [
	/contemporanea col Giappone/i,
	/Canale 149/i,
	/anteprima|trailer|sigle/i,
	/romics|lucca|cartoomics/i,
	/non sapevi|momenti più belli/i,
	/preferiti/i,
	/yamato(\sshop|.*cinema|.*productions)|play yamato/i,
];

const EXCLUDED_TITLES = [...EXCLUDE_TITLES_YAMATO];

const isAnimeTitle = (title: string) =>
	!EXCLUDED_TITLES.some((pattern) => pattern.test(title));

export default class YtPlaylistsExtractor {
	channelPlaylistPage: string;
	$page: cheerio.Selector;

	constructor(channelPlaylistPage: string) {
		this.channelPlaylistPage = channelPlaylistPage;
		this.$page = cheerio.load(this.channelPlaylistPage);
	}

	/**
	 * Scrape for every playlist in the playlist page of a channel.
	 */
	extractPlaylists(): YtPlaylist[] {
		const $ = this.$page;

		return $('a.yt-uix-sessionlink.yt-uix-tile-link')
			.toArray()
			.map((playlist) => {
				const $playlist = $(playlist);
				const href = $playlist.attr('href');
				return {
					title: $playlist.text().trim(),
					id: href ? YtUtils.extractPlaylistId(href) : undefined,
				};
			})
			.filter((playlist): playlist is YtPlaylist =>
				Boolean(playlist.id && playlist.title && isAnimeTitle(playlist.title)),
			);
	}
}
