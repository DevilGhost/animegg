import { isAbsoluteUrl } from '../../utils/net';
import { AnimeOrigin } from '../types';

function extractQueryParam(url: string, queryParam: string) {
	const isRelative = !isAbsoluteUrl(url);
	const urlObj = new URL(url, isRelative ? 'http://example.com' : undefined);
	return urlObj.searchParams.get(queryParam);
}

export default class YtUtils {
	/**
	 * `https://www.youtube.com/playlist?list=playlistId`
	 */
	static playlistUrlFromId(playlistId: string): string {
		return `https://www.youtube.com/playlist?list=${playlistId}`;
	}

	/**
	 * `https://www.youtube.com/watch?v=videoId`
	 */
	static videoUrlFromId(videoId: string): string {
		return `https://www.youtube.com/watch?v=${videoId}`;
	}

	/**
	 * `https://www.youtube.com/channel/channelId`
	 */
	static channelUrlFromId(channelId: string): string {
		return `https://www.youtube.com/channel/${channelId}`;
	}

	/**
	 * Extract the ID of the playlist from a url which have the
	 * query param "list" (usually).
	 *
	 * @example
	 * 	url: "https://www.youtube.com/watch?v=ULv51zuhlDk&list=PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh"
	 * 	id extracted: "PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh"
	 */
	static extractPlaylistId(url: string): string {
		const id = extractQueryParam(url, 'list');

		if (!id) {
			throw new Error(`impossibile estrarre id da url della playlist:\n${url}`);
		}

		return id;
	}

	/**
	 * Extract the ID of the video from a url which have the
	 * query param "v" (usually).
	 *
	 * @example
	 * 	url: "https://www.youtube.com/watch?v=ULv51zuhlDk&list=PL8jk9jEnr_73zJAjpQlaj8O5JZqG5yehh"
	 * 	id extracted: "ULv51zuhlDk"
	 * @param {String} url any url of any video
	 * @return {String} video id
	 */
	static extractVideoId(url: string): string {
		// const matches = url.match(/watch\?v=([0-9A-Za-z_-]{11})&/);
		// return (matches) ? matches[1] : '';
		const id = extractQueryParam(url, 'v');

		if (!id) {
			throw new Error(`impossibile estrarre id da url del video:\n${url}`);
		}

		return id;
	}

	/**
	 * Torna l'url composto solamente da origin e pathname.
	 * Rimuove i query params e l'hash dalla coda in pratica.
	 * @see https://nodejs.org/api/url.html#url_url_strings_and_url_objects
	 * @param url
	 */
	static cleanUrl(url: string) {
		const urlObj = new URL(url);
		return `${urlObj.origin}${urlObj.pathname}`;
	}

	static calculateAnimeId(playlistId: string): string {
		return `${AnimeOrigin.YOUTUBE}-${playlistId}`;
	}

	static calculateEpisodeId(videoId: string): string {
		return `${AnimeOrigin.YOUTUBE}-${videoId}`;
	}
}
