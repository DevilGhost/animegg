import { makeAutoObservable } from 'mobx';
import fs from 'fs-extra';
import path from 'path';
import { remote } from 'electron';
import { DownloadState, DownloadStage, Download } from '../downloader/types';
import { Episode, EpisodeType } from '../search/types';
import { MOCK_DOWNLOAD } from '../constants';
import { FakeDownload } from '../downloader/FakeDownload';
import { YoutubeDlDownload } from '../downloader/youtubedl/YoutubeDlDownload';
import { AnimeDownloaderStore } from './AnimeDownloaderStore';
import { AnimeDownload } from './AnimeDownload';

export class EpisodeDownload implements Episode {
	readonly id: string;
	state: DownloadState = {
		stage: DownloadStage.READY,
		sizeDownloaded: 0,
	};
	filepath: string;

	private episode: Episode;
	private anime: AnimeDownload;
	private store: AnimeDownloaderStore;
	private download?: Download = undefined;

	constructor({
		episode,
		anime,
		store,
	}: {
		anime: AnimeDownload;
		episode: Episode;
		store: AnimeDownloaderStore;
	}) {
		this.id = episode.id;
		this.anime = anime;
		this.episode = episode;
		this.store = store;

		const index = `${episode.index}`.padStart(2, '0');
		const filename = `${this.anime.title} - E${index} ${this.episode.title}.mp4`;
		this.filepath = path.resolve(this.anime.dir, filename);

		makeAutoObservable(this);
	}

	get stage(): DownloadStage {
		return this.state.stage;
	}

	get title(): string {
		return this.episode.title;
	}

	get index(): number {
		return this.episode.index;
	}

	get url(): string {
		return this.episode.url;
	}

	get animeId(): string {
		return this.episode.animeId;
	}

	get episodeId(): string {
		return this.episode.id;
	}

	get isDownloaded(): boolean {
		return !!this.filepath && this.stage === DownloadStage.COMPLETED;
	}

	get isInQueue(): boolean {
		return this.store.queue.has(this.id);
	}

	get groupTitle(): string | undefined {
		return this.episode.groupTitle;
	}

	get type(): EpisodeType {
		return this.episode.type;
	}

	refresh() {
		if (!this.download) {
			return;
		}

		const state = this.download.getState();
		this.state = state;
	}

	start() {
		this.state = {
			sizeDownloaded: 0,
			stage: DownloadStage.DOWNLOADING,
		};
		const download = this.createDownload();
		download.run().then(() => {
			// logger.debug(`download ${id} -> ${stage}`);
			this.filepath = download.filepath;
			this.refresh();
			this.store.onEpisodeDownloadTerminated();
		});
		this.download = download;
		// TODO: manage in queue?
	}

	stop() {
		this.download?.cancel();
		this.state = {
			sizeDownloaded: 0,
			stage: DownloadStage.READY,
		};
		// TODO: manage in queue?
	}

	openFile() {
		try {
			if (!this.isDownloaded || !fs.existsSync(this.filepath)) {
				throw new Error(`episode file at ${this.filepath} does not exists`);
			}

			remote.shell.showItemInFolder(this.filepath);
		} catch (err) {
			// TODO gestione file mancante, fallimento apertura, ecc
		}
	}

	updateEpisodeData(data: Episode) {
		this.episode = data;
	}

	private createDownload() {
		if (MOCK_DOWNLOAD) {
			return new FakeDownload(this.id);
		}

		return new YoutubeDlDownload({
			id: this.id,
			url: this.episode.url,
			filepath: this.filepath,
		});
	}
}
