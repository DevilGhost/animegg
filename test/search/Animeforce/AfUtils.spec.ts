import test from 'ava';
import AfUtils from '../../../src/search/Animeforce/AfUtils';

test('normalizeUrl - adds "http:" where missing', t => {
	const normalizedUrl =
		'http://www.animeforce.org/ds.php?file=Barakamon/Barakamon_Ep_01_SUB_ITA.mp4';
	const malformedUrl =
		'//www.animeforce.org/ds.php?file=Barakamon/Barakamon_Ep_01_SUB_ITA.mp4';
	t.is(AfUtils.normalizeUrl(malformedUrl), normalizedUrl);
});

test('normalizeUrl - convert relative url to absolute animeforce.org url', t => {
	const normalizedUrl =
		'http://www.animeforce.org/ds.php?file=Barakamon/Barakamon_Ep_01_SUB_ITA.mp4';
	const relativeUrl = '/ds.php?file=Barakamon/Barakamon_Ep_01_SUB_ITA.mp4';
	t.is(AfUtils.normalizeUrl(relativeUrl), normalizedUrl);
});

test('extractBaseUrl - extract the base url from a common episode download url', t => {
	const baseUrl = 'http://www.lacumpa.org/DDL/ANIME/';
	const downloadUrl =
		'http://www.lacumpa.org/DDL/ANIME/12SaiChic...kimeki_Ep_01_SUB_ITA.mp4';
	t.is(AfUtils.extractBaseUrl(downloadUrl), baseUrl);
});

test('cleanTitle - strip title tail "Sub Ita Download & Streaming"', t => {
	const correctTitle = 'Barakamon';
	const title = 'Barakamon Sub Ita Download & Streaming';
	t.is(AfUtils.cleanTitle(title), correctTitle);
});

test('fixTitle - replace "[email protected]" with "Idolm@ster"', t => {
	const correctTitle = 'The Idolm@ster: Shiny Festa';
	const title = 'The [email protected]: Shiny Festa';
	t.is(AfUtils.fixTitle(title), correctTitle);
});

test('substituteHost - substitute the host in the download url', t => {
	// host esempio; ce ne sono molti usati
	const host = 'http://www.lacumpa.org/DDL/ANIME/';
	const substituted =
		'http://www.lacumpa.org/DDL/ANIME/12SaiChic...kimeki_Ep_01_SUB_ITA.mp4';
	const download =
		'//www.animeforce.org/ds016.php?file=12SaiChic...kimeki_Ep_01_SUB_ITA.mp4';
	t.is(AfUtils.substituteHost(download, host), substituted);
});

test('isEpisode - recognize common episode title patterns', t => {
	const episodeTitles = [
		'Episodio 01',
		'Episodio 11',
		'Episodio 11.5',
		'Episodio 10/20',
		'Episodio tre',
		'03',
		'3',
	];

	const notEpisodesTitles = ['Film', 'Film 1', 'OAV', 'Special', 'Extra'];

	episodeTitles.forEach(title => {
		t.truthy(AfUtils.isEpisode(title), title);
	});

	notEpisodesTitles.forEach(title => {
		t.falsy(AfUtils.isEpisode(title), title);
	});
});
